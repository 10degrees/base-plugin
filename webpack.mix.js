let mix = require("laravel-mix");
require("laravel-mix-clean");
require("laravel-mix-versionhash");
let wpPot = require("wp-pot");

mix
    // initial mix config
    .clean()
    .setPublicPath("dist")
    .sourceMaps(true, "source-map")
    .webpackConfig({
        externals: {
            jquery: ["$", "jQuery"],
        },
    })
    .options({
        processCssUrls: false,
    })

    // compile assets
    .sass("resources/scss/app.scss", "css")
    .js("resources/js/app.js", "js")
    .copy("resources/fonts", "dist/fonts");

// run for production build
if (mix.inProduction()) {
    mix.versionHash({
        delimiter: "-",
    });
    // generate pot file
    wpPot({
        destFile: "lang/@textdomain.pot",
        domain: "@textdomain",
        package: "@plugin",
        src: ["**/*.php", "!vendor/**"],
    });
}
