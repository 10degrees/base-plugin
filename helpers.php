<?php

use TenDegrees\Foundation\Mix;
use TenDegrees\Routing\ResponseFactory;
use TenDegrees\Support\Facades\App;
use TenDegrees\Support\Facades\URL;
use TenDegrees\Support\Facades\View;

if (!function_exists('td_redirect')) {
    /**
     * Redirect a request
     *
     * @param string $to
     * @param int $status
     * @return void
     */
    function td_redirect(string $to, int $status = 302)
    {
        return td_url()->redirect($to, $status);
    }
}

if (!function_exists('td_url')) {
    /**
     * Generate a url for the application.
     *
     * @param string|null $path
     * @return \TenDegrees\Routing\UrlGenerator|string
     */
    function td_url(string $path = null)
    {
        if (is_null($path)) {
            return URL::getFacadeRoot();
        }

        return URL::to($path);
    }
}

if (!function_exists('td_view')) {
    /**
     * Return a view
     *
     * @param string $view
     * @param array $args
     * @return string
     */
    function td_view(string $view, array $args = []): string
    {
        return View::make($view, $args)->render();
    }
}

if (!function_exists('td_response')) {
    /**
     * Return a new response from the application.
     *
     * @param  \TenDegrees\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \TenDegrees\Http\Response|\TenDegrees\Routing\ResponseFactory
     */
    function td_response($content = null, int $status = 200, array $headers = [])
    {
        $factory = App::make(ResponseFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }
}

if (!function_exists('td_mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @return string
     *
     * @throws \Exception
     */
    function td_mix(string $path)
    {
        return App::make(Mix::class)($path, plugin_dir_url(__FILE__));
    }
}
