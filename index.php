<?php

/**
 * Plugin Name:  @plugin
 * Plugin URI:   https://10degrees.uk
 * Description:  10 Degrees plugin
 * Version:      1.0.0
 * Author:       10 Degrees
 * Author URI:   https://10degrees.uk
 * Text Domain:  @textdomain
 * Domain Path:  /lang
 */

require __DIR__ . '/vendor/autoload.php';

/**
 * Create The Application
 */
$app = new TenDegrees\Foundation\Application(__DIR__);

/**
 * Register Service Providers
 */
$app->register(TenDegrees\Database\DatabaseServiceProvider::class);
$app->register(TenDegrees\Mail\MailServiceProvider::class);
$app->register(TenDegrees\Routing\RoutingServiceProvider::class);
$app->register(TenDegrees\View\ViewServiceProvider::class);
$app->register(App\Providers\AcfServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(App\Providers\RouteServiceProvider::class);
$app->register(App\Providers\WordPressServiceProvider::class);

/**
 * Run The Application
 */
$app->boot();
