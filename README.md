# 10 Degrees Base Plugin

The 10 Degrees base plugin is a starter plugin using the `td/framework` package to streamline WordPress plugin development.

## Installation

From the `wp-content/plugins` directory of your project:

```
composer create-project td/plugin [plugin-name]
```

Find and replace `@plugin` and `@textdomain` to your plugin name and textdomain respectively.

### NPM

The plugin comes with a basic laravel-mix setup.

```
npm install
```
There are some commands to run a dev build, production build, or to watch files:
```
npm run dev
npm run prod
npm run watch
```

## Using the framework

The `td/framework` package contains some useful `wp-cli` commands. View the [framework readme](https://bitbucket.org/10degrees/base-framework/src/main/README.md) for more information on using the framework.

## Deployment

To deploy the plugin, you will need to run the following actions from within the plugin directory.

```
composer install
npm install
npm run prod
```