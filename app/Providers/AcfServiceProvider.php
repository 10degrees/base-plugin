<?php

namespace App\Providers;

use TenDegrees\Foundation\Providers\AcfServiceProvider as ServiceProvider;

class AcfServiceProvider extends ServiceProvider
{
    /**
     * The blocks to register
     *
     * @var array
     */
    protected $blocks = [
        //
    ];

    /**
     * The field groups to register
     *
     * @var array
     */
    protected $fieldGroups = [
        //
    ];
}
