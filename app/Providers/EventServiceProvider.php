<?php

namespace App\Providers;

use App\Listeners\EnqueueScripts;
use TenDegrees\Foundation\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The hooks (actions/filters) to "listen" to.
     *
     * @var array
     */
    protected $hooks = [
        'wp_enqueue_scripts' => [
            EnqueueScripts::class,
        ],
    ];

    /**
     * The subscribers. These are passed the event dispatcher instance.
     *
     * @var array
     */
    protected $subscribers = [
        //
    ];
}
