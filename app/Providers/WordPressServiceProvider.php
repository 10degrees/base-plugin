<?php

namespace App\Providers;

use TenDegrees\Foundation\Providers\WordPressServiceProvider as ServiceProvider;

class WordPressServiceProvider extends ServiceProvider
{
    /**
     * The post types to register
     *
     * @var array
     */
    protected $postTypes = [
        //
    ];

    /**
     * The shortcodes to register
     *
     * @var array
     */
    protected $shortcodes = [
        //
    ];

    /**
     * The admin pages to register
     *
     * @var array
     */
    protected $adminPages = [
        //
    ];
}
