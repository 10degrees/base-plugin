<?php

namespace App\Listeners;

class EnqueueScripts
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle()
    {
        // enqueue the plugin css
        wp_enqueue_style('@plugin', td_mix('css/app.css'), [], null, 'all');

        // enqueue the plugin js
        wp_enqueue_script('@plugin', td_mix('js/app.js'), ['jquery'], null, true);
    }
}
